TIMESTAMP=$(date +%s)
DOMAIN=$DOMAIN

function usage() {
    echo Need value for DOMAIN
    exit 1
}

if [ -z $DOMAIN ]
then
    usage
    exit 1
fi

echo Using timestamp $TIMESTAMP
echo Using domain $DOMAIN
echo Service will be called nginx-ssl-$TIMESTAMP.$DOMAIN

helm3 upgrade --install --create-namespace --atomic --wait \
    --namespace nginx-ssl-service-$TIMESTAMP \
    nginx-ssl-service-$TIMESTAMP \
    ./nginx-ssl-service \
    --set ingress.enabled=true \
    --set ingress.annotations."kubernetes\.io/ingress\.class"=nginx \
    --set ingress.annotations."cert-manager\.io/cluster-issuer"=letsencrypt-production \
    --set ingress.hosts[0].host="nginx-ssl-$TIMESTAMP.$DOMAIN" \
    --set ingress.hosts[0].paths[0].path="/" \
    --set ingress.hosts[0].paths[0].pathType="ImplementationSpecific" \
    --set ingress.tls[0].secretName="nginx-ssl-tls-$TIMESTAMP" \
    --set ingress.tls[0].hosts[0]="nginx-ssl-$TIMESTAMP.$DOMAIN" \
    --debug \
    --dry-run \
    | tee debug.log

if [ $? -ne 0 ]; then
  echo Dry run failed, check above and debug.log
  exit 1
fi

helm3 upgrade --install --create-namespace --atomic --wait \
    --namespace nginx-ssl-service-$TIMESTAMP \
    nginx-ssl-service-$TIMESTAMP \
    ./nginx-ssl-service \
    --set ingress.enabled=true \
    --set ingress.annotations."kubernetes\.io/ingress\.class"=nginx \
    --set ingress.annotations."cert-manager\.io/cluster-issuer"=letsencrypt-production \
    --set ingress.hosts[0].host="nginx-ssl-$TIMESTAMP.$DOMAIN" \
    --set ingress.hosts[0].paths[0].path="/" \
    --set ingress.hosts[0].paths[0].pathType="ImplementationSpecific" \
    --set ingress.tls[0].secretName="nginx-ssl-tls-$TIMESTAMP" \
    --set ingress.tls[0].hosts[0]="nginx-ssl-$TIMESTAMP.$DOMAIN"